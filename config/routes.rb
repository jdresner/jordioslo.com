Rails.application.routes.draw do
  scope "(:locale)/", locale: /#{I18n.available_locales.join("|")}/ do
    root 'pages#index'
    get '/store' => 'pages#store', as: :store  
    get '/contact' => 'pages#contact', as: :contact   

    get 'strip/random' => 'strips#random', as: :random_strip
    get 'strip/archive' => 'strips#index', as: :archive
    get 'strips/archive' => 'strips#index'
    get 'strip/:id' => 'strips#show', as: :strip
    get 'strips/:id' => 'strips#show'
    
    get 'blog' => 'blogposts#index', as: :blog
    get 'blog/:id' => 'blogposts#show', as: :blogpost
   
    get '/rss' => 'pages#feed', as: :feed, defaults: { format: 'rss' }
    get '/feed' => 'pages#feed', defaults: { format: 'rss' }
    get '/feed.rss' => 'pages#feed', defaults: { format: 'rss' }

    devise_for :admins
    get 'admin' => 'admin/pages#index', as: :admin_root
    namespace :admin do
      resources :strips, :blogposts, :file_uploads
    end
  end

  mount Ckeditor::Engine => '/ckeditor'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
