require 'test_helper'

class Admin::StripsControllerTest < ActionController::TestCase
  setup do
    @admin_strip = admin_strips(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_strips)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_strip" do
    assert_difference('Admin::Strip.count') do
      post :create, admin_strip: {  }
    end

    assert_redirected_to admin_strip_path(assigns(:admin_strip))
  end

  test "should show admin_strip" do
    get :show, id: @admin_strip
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_strip
    assert_response :success
  end

  test "should update admin_strip" do
    patch :update, id: @admin_strip, admin_strip: {  }
    assert_redirected_to admin_strip_path(assigns(:admin_strip))
  end

  test "should destroy admin_strip" do
    assert_difference('Admin::Strip.count', -1) do
      delete :destroy, id: @admin_strip
    end

    assert_redirected_to admin_strips_path
  end
end
