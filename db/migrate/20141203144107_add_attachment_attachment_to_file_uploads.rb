class AddAttachmentAttachmentToFileUploads < ActiveRecord::Migration
  def self.up
    change_table :file_uploads do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :file_uploads, :attachment
  end
end
