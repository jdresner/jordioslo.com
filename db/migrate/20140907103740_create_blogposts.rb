class CreateBlogposts < ActiveRecord::Migration
  def change
    create_table :blogposts do |t|
      t.string :title
      t.text :body
      t.date :publish_date
      t.boolean :published, default: false

      t.timestamps
    end
  end
end
