class CreateStrips < ActiveRecord::Migration
  def change
    create_table :strips do |t|
      t.string :title
      t.date :publish_date, default: Date.today
      t.boolean :published, default: false

      t.timestamps
    end
  end
end
