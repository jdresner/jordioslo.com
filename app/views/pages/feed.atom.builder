atom_feed :language => 'en-US' do |feed|
  feed.title @title

  @feed_items.each do |item|
    next if item.publish_date.blank?

    feed.entry(item) do |entry|
      if item.class == Strip
        entry.url strip_url(item)
      else
        entry.url blogpost_url(item)
      end

      entry.title item.title

      if item.class == Strip
        entry.content link_to(image_tag(item.image.url(:full)), strip_url(item)), :type => 'html'
      else
        entry.content item.body, :type => 'html'
      end
      

      # the strftime is needed to work with Google Reader.
      entry.updated(item.publish_date.strftime("%Y-%m-%dT%H:%M:%SZ")) 

      # entry.author do |author|
      #   author.name entry.author_name
      # end
    end
  end
end