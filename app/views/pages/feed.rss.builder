#atom_feed :language => 'en-US' do |feed|
#  feed.title @title

#  @feed_items.each do |item|
#    next if item.publish_date.blank?

#    feed.entry(item) do |entry|
#      if item.class == Strip
#        entry.url strip_url(item)
#      else
#        entry.url blogpost_url(item)
#      end

#      entry.title item.title

#      if item.class == Strip
#        entry.content link_to(image_tag(item.image.url(:full)), strip_url(item)), :type => 'html'
#      else
#        entry.content item.body, :type => 'html'
#      end
      

      # the strftime is needed to work with Google Reader.
#      entry.updated(item.publish_date.strftime("%Y-%m-%dT%H:%M:%SZ")) 

      # entry.author do |author|
      #   author.name entry.author_name
      # end
#    end
#  end
#end

xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title t("site.title")
    xml.description t("site.description")
    xml.link root_url
    xml.language 'ca'
    #xml.tag! 'atom:link', :href => feed_url, :rel => 'self', :type => 'application/rss+xml'

    for feed_item in @feed_items
      xml.item do
        xml.title feed_item.title
        if feed_item.class == Strip
          xml.description image_tag(image_url(feed_item.image.url(:full)))
        else
          xml.description feed_item.body
        end
        xml.pubDate feed_item.publish_date.strftime('%a, %d %b %Y %H:%M:%S %z')
        if feed_item.class == Strip
          xml.link strip_url(feed_item)
        else
          xml.link blogpost_url(feed_item)
        end
        if feed_item.class == Strip
          xml.guid strip_url(feed_item)
        else
          xml.guid blogpost_url(feed_item)
        end
      end
    end
  end
end