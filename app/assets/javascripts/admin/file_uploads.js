  $(document).ready(function(){
    // disable auto discover
    Dropzone.autoDiscover = true;
     
    // grap our upload form by its id
    $("#new_file_upload").dropzone({
      // changed the passed param to one accepted by
      // our rails app
      paramName: "file_upload[attachment]",
      // show remove links on each image upload
      addRemoveLinks: true,
      uploadMultiple: false,
      parallelUploads: 1
    }); 
  });
