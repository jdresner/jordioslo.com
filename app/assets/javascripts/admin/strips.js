  $(document).ready(function(){
    // disable auto discover
    Dropzone.autoDiscover = true;
     
    // grap our upload form by its id
    $("#new_strip").dropzone({
      // changed the passed param to one accepted by
      // our rails app
      paramName: "strip[image]",
      // show remove links on each image upload
      addRemoveLinks: true,
      uploadMultiple: false,
      parallelUploads: 1
    }); 
  });
