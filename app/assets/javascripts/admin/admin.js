$(function() {
  $( ".datepicker" ).datepicker({
    showAnim:'blind',
    dateFormat: "yy-mm-dd",
    changeMonth: true,
    changeYear: true
  });
});