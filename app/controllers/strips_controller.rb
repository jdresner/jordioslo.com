class StripsController < ApplicationController
  before_action :set_page_id
  before_action :set_strip, only: [:show]
  before_action :set_links, only: [:show]

  def index
    @page_title = "Archive"
    @strips = Strip.published
    @strip_months = @strips.group_by { |s| s.publish_date.beginning_of_month }
    @popular_tags = ActsAsTaggableOn::Tag.most_used(10)

    #if params[:q]
    #  tag_results = Strip.published.tagged_with(params[:q].split(" "), :any => true, :wild => true) 
    #  title_results = Strip.published.search "#{params[:q]}"
    #  @results = tag_results | title_results
    #  @results_months = @results.group_by { |r| r.publish_date.beginning_of_month }
    #end
  end

  def show
    if @strip
      @page_title = @strip.title
      @related_strips = @strip.find_related_tags.published.limit(5)
    end
  end

  def random
    random_strip = Strip.offset(rand(Strip.count)).first
    redirect_to random_strip
  end

  private
    def set_page_id
      @page_id = "archive"
    end
    
    def set_strip
      if params[:id]
        @strip = Strip.published.find(params[:id])
      else
        @strip = Strip.published.first
      end
    end

    def set_links
      if @strip
        @next_strip = @strip.next
        @previous_strip = @strip.previous
        @first_strip = Strip.published.earliest unless @strip == Strip.published.earliest
        @latest_strip = Strip.published.latest unless @strip == Strip.published.latest
      end
    end
end
