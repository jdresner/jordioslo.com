class Admin::StripsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_strip, only: [:show, :edit, :update, :destroy]
  layout "admin"

  def index
    @strip = Strip.new
    @strips = Strip.all
  end

  def show
  end

  def new
    @strip = Strip.new(publish_date: Date.today, published: true)
    @manual = true
  end

  def edit
    @manual = true
  end

  def create
    @strip = Strip.new(strip_params)
    respond_to do |format|
      if @strip.save
        format.html { redirect_to admin_strip_path(@strip), notice: 'Strip was successfully created.' }
        format.json { render :json => {:message => "Success"} }
      else
        format.html { render :new }
        format.json { render json: @strip.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @strip.update(strip_params)
        format.html { redirect_to admin_strips_path, notice: 'Strip was successfully updated.' }
        format.json { render :show, status: :ok, location: @strip }
      else
        format.html { render :edit }
        format.json { render :json => {:message => "Success"} }
      end
    end
  end

  def destroy
    @strip.destroy
    respond_to do |format|
      format.html { redirect_to admin_strips_url, notice: 'Strip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_strip
      @strip = Strip.find(params[:id])
    end

    def strip_params
      params[:strip].permit(:image, :title, :publish_date, :published, :tag_list)
    end
end
