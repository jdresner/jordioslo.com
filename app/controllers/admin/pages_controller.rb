class Admin::PagesController < ApplicationController
  before_action :authenticate_admin!
  
  layout "admin"
  
  def index
    @visits = Visit.group_by_day(:started_at, format: "%B %e, %Y").count
    @total_visits = Visit.count
  end
end
