class Admin::FileUploadsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_file_upload, only: [:show, :edit, :update, :destroy]
  layout "admin"

  def index
    @file_upload = FileUpload.new
    @file_uploads = FileUpload.all
  end

  def show
  end

  def new
    @file_upload = FileUpload.new
  end

  def edit
  end

  def create
    @file_upload = FileUpload.new(file_upload_params)

    respond_to do |format|
      if @file_upload.save
        format.html { redirect_to admin_file_uploads_url, notice: 'Upload was successfully created.' }
        format.json { render :json => {:message => "Success"} }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @file_upload.update(file_upload_params)
        format.html { redirect_to admin_file_uploads_url, notice: 'Upload was successfully updated.' }
        format.json { render :json => {:message => "Success"} }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @file_upload.destroy
    respond_to do |format|
      format.html { redirect_to admin_file_uploads_url, notice: 'Upload was successfully destroyed.' }
    end
  end

  private
    def set_file_upload
      @file_upload = FileUpload.find(params[:id])
    end

    def file_upload_params
      params.require(:file_upload).permit(:attachment)
    end
end
