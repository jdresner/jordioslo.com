class Admin::BlogpostsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_blogpost, only: [:show, :edit, :update, :destroy]
  layout "admin"

  def index
    @blogposts = Blogpost.all
  end

  def show
  end

  def new
    @blogpost = Blogpost.new(publish_date: Date.today, published: true)
  end

  def edit
  end

  def create
    @blogpost = Blogpost.new(blogpost_params)

    respond_to do |format|
      if @blogpost.save
        format.html { redirect_to admin_blogpost_path(@blogpost), notice: 'Blogpost was successfully created.' }
        format.json { render :show, status: :created, location: @blogpost }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @blogpost.update(blogpost_params)
        format.html { redirect_to admin_blogpost_path(@blogpost), notice: 'Blogpost was successfully updated.' }
        format.json { render :show, status: :ok, location: @blogpost }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @blogpost.destroy
    respond_to do |format|
      format.html { redirect_to admin_blogposts_url, notice: 'Blogpost was successfully destroyed.' }
    end
  end

  private
    def set_blogpost
      @blogpost = Blogpost.find(params[:id])
    end

    def blogpost_params
      params.require(:blogpost).permit(:title, :body, :publish_date, :published, :tag_list)
    end
end
