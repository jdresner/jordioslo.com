class BlogpostsController < ApplicationController
  before_action :set_page_id
  before_action :set_blogpost, only: [:show]

  def index
    @page_title = "Blog"
    @blogposts = Blogpost.published
    @blogpost_months = @blogposts.group_by { |b| b.publish_date.beginning_of_month }

    #if params[:q]
    #  @results = Blogpost.published.search "#{params[:q]}"
    #  @results_months = @results.group_by { |r| r.publish_date.beginning_of_month }
    #end
  end

  def show
    @page_title = @blogpost.title
  end

  private
    def set_page_id
      @page_id = "blog"
    end

    def set_blogpost
      if params[:id]
        @blogpost = Blogpost.published.find(params[:id])
      else
        @blogpost = Blogpost.published.first
      end
    end
end
