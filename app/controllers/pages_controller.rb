class PagesController < ApplicationController
  def index
    @page_id = "home"

    @strip = Strip.latest
    
    latest_blogpost = Blogpost.published.first
    date = latest_blogpost.publish_date if latest_blogpost
    @blogposts = Blogpost.all.where("publish_date = ?", date)

    @page_title = @strip.title if @strip

    if @strip
      @next_strip = @strip.next
      @previous_strip = @strip.previous
      @first_strip = Strip.earliest unless @strip == Strip.earliest
      @latest_strip = Strip.latest unless @strip == Strip.latest
    end
  end

  def store
    @page_id = "store"
    @page_title = "Store"
  end

  def contact
    @page_id = "contact"
    @page_title = "Contact"
  end

  def feed
    @title = "Jordi & Oslo by Guillem Ruiz"
    strips = Strip.published
    blogposts = Blogpost.published
    @feed_items = (strips + blogposts)
    @feed_items.sort! { |b,a| a.publish_date <=> b.publish_date }
    @updated = @feed_items.first.publish_date unless @feed_items.empty?

    respond_to do |format|
      format.rss { render layout: false  }
    end
  end
end
