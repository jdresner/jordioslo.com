class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_locale

private
  
  def set_locale
    if params[:locale].present? 
      I18n.locale = params[:locale]  
    else
      I18n.locale = extract_locale_from_accept_language_header
    end
  end

  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  def extract_locale_from_accept_language_header
    begin
      request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    rescue
      'ca'
    end
  end
end
