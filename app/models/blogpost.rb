class Blogpost < ActiveRecord::Base
  #after_save :reindex  
  
  #searchkick

  default_scope { order('publish_date desc') }
  scope :published, -> { where(published: true).where('publish_date <= ?', Date.today) }

  def to_param
    [id, title.parameterize].join("-")
  end

  def next
    self.class.published.where("publish_date > ?", publish_date).last
  end

  #def reindex
  #  Blogpost.reindex
  #end
end
