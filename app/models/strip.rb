class Strip < ActiveRecord::Base
  after_create :extract_filename_data, :increment_publish_date
  before_create :get_latest_strip, :publish
  #after_save :reindex

  acts_as_ordered_taggable

  #searchkick
  
  has_attached_file :image,
                    :styles => { :full => "1000x5000>", :thumb => "600x600>" },
                    :url => "/files/strip/:id/:style/:filename",
                    :path => ":rails_root/public/files/strip/:id/:style/:filename",
                    :use_timestamp => false
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  def next
    self.class.published.where("publish_date > ?", publish_date).last
  end

  def previous
    self.class.published.where("publish_date < ?", publish_date).first
  end

  def self.latest
    published.first
  end

  def self.earliest
    published.last
  end

  default_scope { order('publish_date desc') }
  scope :published, -> { where(published: true).where('publish_date <= ?', Date.today) }

  private
    def extract_filename_data
      unless @manual
        filename = File.basename(self.image_file_name, File.extname(self.image_file_name))
        filename.slice!(0..5)

        new_tags = filename.scan(/tag_\w+/)
        new_tags.each {|t| t.gsub! "tag_", ", "}
        new_tags.each {|t| t.gsub! "_,", ","}
        self.tag_list = new_tags

        filename.gsub!(/tag_\w+/,"")
        filename.gsub!('__', ', ')
        filename.slice!((filename.index('_ó'))..-1) if filename.index('_ó')    
        filename.gsub!('_', ' ')
   
        self.title = filename

        self.save
      end
    end

    def get_latest_strip
      @latest_strip = Strip.first
    end

    def increment_publish_date
      unless @manual or !@latest_strip
        self.publish_date = @latest_strip.publish_date + 2.days
        self.save
      end
    end

    def publish
      self.published = true
    end
    
    #def reindex
    #  Strip.reindex
    #end
end
