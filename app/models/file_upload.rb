class FileUpload < ActiveRecord::Base
  has_attached_file :attachment,
                    :url => "/files/uploads/:id/:filename",
                    :path => ":rails_root/public/files/uploads/:id/:filename",
                    :use_timestamp => false
  validates_attachment_presence :attachment
  do_not_validate_attachment_file_type :attachment

  default_scope { order('id desc') }
end
